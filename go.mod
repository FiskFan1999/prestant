module codeberg.org/FiskFan1999/prestant

go 1.19

require (
	codeberg.org/FiskFan1999/gemini v0.3.1
	github.com/spf13/pflag v1.0.5
)

require github.com/madflojo/testcerts v1.0.1 // indirect
