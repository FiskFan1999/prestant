/*
Copyright (C) 2022 William Rehwinkel

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"fmt"
	"os"
	"path"
	"time"
)

var cleanupTicker = time.Tick(time.Hour)

func CleanupMain() {
	for _ = range cleanupTicker {
		DoCleanup()
	}
}

func DoCleanup() {
	/*
		For all files, if they are older than Lifetime, delete.
	*/
	files, _ := os.ReadDir(Dir)
	for _, file := range files {
		fileInfo, err := file.Info()
		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		if time.Since(fileInfo.ModTime()) > Lifetime {
			os.Remove(path.Join(Dir, file.Name()))
		}
	}
}
