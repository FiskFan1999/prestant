# prestant

Gemini protocol pastebin, allowing users to temporarily upload their files to share with others on platforms such as chat rooms.

## How to upload files
Upload requests should be directed at `gemini://example.com/upload/` with the query parameter being the URL-escaped contents of the file. After this you will be redirected to the direct link to this file, which you can share with others. Note that all uploaded content will be removed after a certain period of time. Please consider other platforms for more permanent filehosting.
