/*
Copyright (C) 2022 William Rehwinkel

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"crypto/tls"
	"fmt"
	"net/url"
	"os"
	"path"
	"strings"

	"codeberg.org/FiskFan1999/gemini"
)

var (
	NotReady = gemini.ResponseFormat{
		Status: gemini.TemporaryFailure,
		Mime:   "Not ready",
		Lines:  nil,
	}
	BadRequest = gemini.ResponseFormat{
		Status: gemini.BadRequest,
		Mime:   "Bad request",
		Lines:  nil,
	}
)

func root() gemini.Response {
	file, err := os.Open("README.md")
	if err != nil {
		return gemini.ResponseFormat{
			Status: gemini.TemporaryFailure,
			Mime:   err.Error(),
			Lines:  nil,
		}
	}
	return gemini.ResponseRead{
		file,
		"text/markdown",
		"root",
	}
}

func read(u *url.URL, c *tls.Conn) gemini.Response {
	parts := strings.FieldsFunc(u.EscapedPath(), func(r rune) bool { return r == '/' })
	f := parts[0]

	file, err := os.Open(path.Join(Dir, f))
	if err != nil {
		return BadRequest
	}

	return gemini.ResponseRead{
		Content: file,
		Mime:    "text/plain",
		Name:    "paste content",
	}
}

func upload(u *url.URL, c *tls.Conn) gemini.Response {
	if u.RawQuery == "" {
		// input
		return gemini.ResponseFormat{
			gemini.Input,
			"text",
			nil,
		}
	}
	text, err := url.QueryUnescape(u.RawQuery)
	if err != nil {
		return gemini.ResponseFormat{
			Status: gemini.TemporaryFailure,
			Mime:   err.Error(),
			Lines:  nil,
		}
	}
	// get tmp file name
	file, err := os.CreateTemp(Dir, "")
	if err != nil {
		return gemini.ResponseFormat{
			Status: gemini.TemporaryFailure,
			Mime:   err.Error(),
			Lines:  nil,
		}
	}
	defer file.Close()
	fmt.Fprint(file, text)
	name := path.Base(file.Name())

	return gemini.ResponseFormat{
		gemini.RedirectTemporary,
		fmt.Sprintf("/%s/", name),
		nil,
	}
}

func handler(u *url.URL, c *tls.Conn) gemini.Response {
	parts := strings.FieldsFunc(u.EscapedPath(), func(r rune) bool { return r == '/' })
	if len(parts) > 1 {
		return BadRequest
	}
	if len(parts) == 1 {
		if parts[0] == "upload" {
			// upload handler
			return upload(u, c)
		} else {
			return read(u, c)
		}
	}
	return root()
}
