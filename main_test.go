/*
Copyright (C) 2022 William Rehwinkel

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"io"
	"math/rand"
	"net/url"
	"os"
	"path"
	"strings"
	"testing"
	"time"

	"github.com/madflojo/testcerts"
)

func FuzzUpload(f *testing.F) {
	f.Add(bytes.Repeat([]byte("a"), 512))
	f.Add(bytes.Repeat([]byte("a"), 1024))
	f.Add(bytes.Repeat([]byte("a"), 1025))
	f.Add(bytes.Repeat([]byte("a"), 5012))

	Dir = "files"
	os.Mkdir(Dir, 0700)
	cert, key, err := testcerts.GenerateCerts()
	if err != nil {
		f.Fatal(err.Error())
	}

	serv := getServer("127.0.0.1:0", cert, key)
	go serv.Run()
	<-serv.Ready

	f.Fuzz(func(t *testing.T, input []byte) {
		inputEscape := url.QueryEscape(string(input))

		u, err := url.Parse(fmt.Sprintf("/upload/?%s", inputEscape))
		if err != nil {
			t.Fatal(err.Error())
		}
		conn, err := tls.Dial("tcp", serv.Address, &tls.Config{
			InsecureSkipVerify: true,
		})
		if err != nil {
			t.Fatal(err.Error())
		}
		defer conn.Close()

		go fmt.Fprintf(conn, "%s\r\n", u)
		outputBytes, err := io.ReadAll(conn)
		if err != nil {
			t.Fatal(err.Error())
		}

		outputStr := string(outputBytes)
		newPath := strings.Fields(outputStr)[1]

		u2, err := url.Parse(newPath)
		if err != nil {
			t.Fatal(err.Error())
		}
		conn, err = tls.Dial("tcp", serv.Address, &tls.Config{
			InsecureSkipVerify: true,
		})
		if err != nil {
			t.Fatal(err.Error())
		}
		defer conn.Close()

		go fmt.Fprintf(conn, "%s\r\n", u2)

		// read first line
		firstLine := make([]byte, len("20 text/plain\r\n"))
		_, err = conn.Read(firstLine)
		if err != nil {
			t.Fatal(err.Error())
		}

		if (len(input) == 0 || int64(len(u.String())) > readLimit) && bytes.Equal(firstLine, []byte("59 Bad request\r")) {
			t.Skip()
		} else if !bytes.Equal(firstLine, []byte("20 text/plain\r\n")) {
			t.Fatalf("Did not recieve correct first line, recieved \"%s\"", firstLine)
		}

		outputBytes2, err := io.ReadAll(conn)
		if err != nil {
			t.Fatal(err.Error())
		}
		if !bytes.Equal(outputBytes2, input) {
			t.Error("Did not recieve output")
		}
		/*
			Delete extra file from folder
		*/
		filename := strings.FieldsFunc(newPath, func(r rune) bool { return r == '/' })[0]
		os.Remove(path.Join(Dir, filename))
	})
}

func TestUpload(t *testing.T) {
	Dir = "files"
	os.Mkdir(Dir, 0700)
	cert, key, err := testcerts.GenerateCerts()
	if err != nil {
		t.Fatal(err.Error())
	}

	serv := getServer("127.0.0.1:0", cert, key)
	go serv.Run()
	<-serv.Ready

	r := rand.New(rand.NewSource(time.Now().UnixMicro()))
	input := make([]byte, 512)
	if _, err := r.Read(input); err != nil {
		t.Fatal(err.Error())
	}
	inputEscape := url.QueryEscape(string(input))

	u, err := url.Parse(fmt.Sprintf("/upload/?%s", inputEscape))
	if err != nil {
		t.Fatal(err.Error())
	}
	conn, err := tls.Dial("tcp", serv.Address, &tls.Config{
		InsecureSkipVerify: true,
	})
	if err != nil {
		t.Fatal(err.Error())
	}
	defer conn.Close()

	go fmt.Fprintf(conn, "%s\r\n", u)
	outputBytes, err := io.ReadAll(conn)
	if err != nil {
		t.Fatal(err.Error())
	}

	outputStr := string(outputBytes)
	newPath := strings.Fields(outputStr)[1]

	u2, err := url.Parse(newPath)
	if err != nil {
		t.Fatal(err.Error())
	}
	conn, err = tls.Dial("tcp", serv.Address, &tls.Config{
		InsecureSkipVerify: true,
	})
	if err != nil {
		t.Fatal(err.Error())
	}
	defer conn.Close()

	go fmt.Fprintf(conn, "%s\r\n", u2)

	// read first line
	firstLine := make([]byte, len("20 text/plain\r\n"))
	_, err = conn.Read(firstLine)
	if err != nil {
		t.Fatal(err.Error())
	}

	if !bytes.Equal(firstLine, []byte("20 text/plain\r\n")) {
		t.Fatalf("Did not recieve correct first line, recieved \"%s\"", firstLine)
	}

	outputBytes2, err := io.ReadAll(conn)
	if err != nil {
		t.Fatal(err.Error())
	}
	if !bytes.Equal(outputBytes2, input) {
		t.Error("Did not recieve output")
	}

	filename := strings.FieldsFunc(newPath, func(r rune) bool { return r == '/' })[0]
	os.Remove(path.Join(Dir, filename))

}
