/*
Copyright (C) 2022 William Rehwinkel

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"log"
	"os"
	"time"

	"codeberg.org/FiskFan1999/gemini"
	flag "github.com/spf13/pflag"
)

const (
	readLimitDef = 4096
	LifetimeDef  = time.Hour * 24 * 30
	DirDef       = "files"
)

var (
	Port      string
	Cert      []byte
	Key       []byte
	CertPath  string
	KeyPath   string
	Dir       string        = DirDef
	readLimit int64         = readLimitDef
	Lifetime  time.Duration = LifetimeDef
)

func getServer(port string, cert, key []byte) *gemini.Server {
	serv := gemini.GetServer(port, cert, key)
	serv.ReadLimit = readLimit
	serv.Handler = handler
	return serv
}

func main() {
	var err error

	flag.StringVarP(&Port, "port", "p", ":1965", "Port to listen on")
	flag.StringVarP(&CertPath, "certificate", "c", "cert.pem", "TLS certificate")
	flag.StringVarP(&KeyPath, "key", "k", "key.pem", "TLS certificate secret")
	flag.StringVarP(&Dir, "directory", "d", DirDef, "Directory to host pasted files in")
	flag.DurationVarP(&Lifetime, "lifetime", "t", LifetimeDef, "Pastebin lifetime. Files will be deleted once they are older than this duration.")
	flag.Int64VarP(&readLimit, "size", "s", readLimitDef, "Maximum input size")
	flag.Parse()

	// open directory. Note that this will ignore
	// the error if the directory already exists

	if err := os.Mkdir(Dir, 0700); err != nil && !os.IsExist(err) {
		log.Fatal(err)
	}

	Cert, err = os.ReadFile(CertPath)
	if err != nil {
		log.Fatal(err.Error())
	}
	Key, err = os.ReadFile(KeyPath)
	if err != nil {
		log.Fatal(err.Error())
	}

	go CleanupMain()

	log.Fatalln(getServer(Port, Cert, Key).Run().Error())

}
