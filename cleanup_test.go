/*
Copyright (C) 2022 William Rehwinkel

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"os"
	"path"
	"testing"
	"time"
)

func TestDoCleanup(t *testing.T) {
	var err error
	Dir, err = os.MkdirTemp("", "")
	if err != nil {
		t.Fatal(err.Error())
	}
	defer os.RemoveAll(Dir)

	//redefine lifetime for testing
	Lifetime = time.Millisecond * 100

	// write files at different times
	if err := os.WriteFile(path.Join(Dir, "1"), []byte{}, 0600); err != nil {
		t.Fatal(err.Error())
	}
	time.Sleep(Lifetime * 2)
	if err := os.WriteFile(path.Join(Dir, "2"), []byte{}, 0600); err != nil {
		t.Fatal(err.Error())
	}

	DoCleanup()

	files, err := os.ReadDir(Dir)
	if err != nil {
		t.Fatal(err.Error())
	}
	if len(files) != 1 {
		t.Fatal("len(files) != 1")
	}
	if files[0].Name() != "2" {
		t.Fatal("files[0].Name() != \"2\"")
	}

}
